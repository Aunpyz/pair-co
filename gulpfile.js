const gulp = require('gulp')
const handler = require('serve-handler')
const http = require('http')
const browserSync = require('browser-sync')
const sass = require('gulp-sass')
const cleanCSS = require('gulp-clean-css')

sass.compiler = require('node-sass')

gulp.task('serve', () => {
    const server = http.createServer((request, response) => {
        return handler(request, response, {
            public: 'dist'
        })
    })
    server.listen(4000)
    const bs = browserSync({
        proxy: 'localhost:4000',
        browser: ['google chrome', 'chromium']
    })
    const handleStop = () => {
        bs.cleanup()
        server.close()
    }
    process.on('SIGTERM', handleStop)
    process.on('SIGINT', handleStop)
})

gulp.task('html', () => {
    return gulp.src('src/**/*.html')
        .pipe(gulp.dest('dist/'))
})

gulp.task('assets', () => {
    return gulp.src(['src/assets/**', '!src/**/sass', '!src/**/sass/**/*'])
        .pipe(gulp.dest('dist/assets'))
})

gulp.task('sass', () => {
    return gulp.src('src/assets/sass/**/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(cleanCSS())
        .pipe(gulp.dest('dist/assets/css'))
        .pipe(browserSync.stream())
})

gulp.task('watch', () => {
    gulp.watch('src/assets/sass/**/*.scss', gulp.series('sass'))
    gulp.watch(['strc/assets/**/*', '!src/assets/sass/**'], gulp.series('assets'))
    gulp.watch('src/**/*.html', gulp.series('html'))
    gulp.watch([
        'src/**/*.html',
        'src/assets/sass/**/*.scss'
    ]).on('change', browserSync.reload)
})

gulp.task('dev', gulp.series(gulp.parallel('html', 'sass', 'assets'), gulp.parallel('serve', 'watch')))